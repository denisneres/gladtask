export class User {
	public id: string;
	public username: string;
	public firstName: string;
	public lastName: string;
	public email: string;
	public password: string;
	public profileEnum: string;
	public profilePhoto: string;

	constructor() {}
}
